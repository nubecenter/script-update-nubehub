#!bin/bash
#actualizar nubehub

url=https://nubecenter.com.ar/downloads/thinclient/
nubehub=nubehub-thin-client.deb
destino_nubehub=/home/pi/Downloads/

destino_autostart=/home/pi/.config/autostart/
nombre_autostart=nubehub.desktop

echo Descargando Nubehub thinclien ...

rm $destino_nubehub/$nubehub

wget $url/$nubehub -P $destino_nubehub

echo instalando ...

sudo dpkg -i  $destino_nubehub/$nubehub

echo Creando autostart ...

mkdir $destino_autostart || touch $destino_autostart/$nombre_autostart

touch $destino_autostart/$nombre_autostart

echo "[Desktop Entry]
Type=Application
Name=Nubehub
Exec=sudo nubehub --no-sandbox" > $destino_autostart/$nombre_autostart
